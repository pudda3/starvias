<?php
    include "cabecalho.php";
?>
    <section id="clientes">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="clientes d-flex flex-column justify-content-between align-items-stretch">
                    <div class="text-center">
                        <b>Clientes</b>
                    </div>
                    <div class="linha d-flex justify-content-between align-items-center">
                        <div>
                            <img class="img-fluid" src="img/clientes/bridgestone.png" alt="">
                        </div>
                        <div>
                            <img class="img-fluid" src="img/clientes/mercedes.png" alt="">
                        </div>
                        <div>
                            <img class="img-fluid" src="img/clientes/embraco.png" alt="">
                        </div>
                        <div>
                            <img class="img-fluid" src="img/clientes/hyundai.png" alt="">
                        </div>
                        <div>
                            <img class="img-fluid" src="img/clientes/fibria.png" alt="">
                        </div>
                    </div>
                    <div class="linha d-flex justify-content-between align-items-center">
                        <div>
                            <img class="img-fluid" src="img/clientes/mrv.png" alt="">
                        </div>
                        <div>
                            <img class="img-fluid" src="img/clientes/honda.png" alt="">
                        </div>
                        <div>
                            <img class="img-fluid" src="img/clientes/majopar.png" alt="">
                        </div>
                        <div>
                            <img class="img-fluid" src="img/clientes/tiete.png" alt="">
                        </div>
                    </div>
                    <div class="linha d-flex justify-content-between align-items-center">
                        <div>
                            <img class="img-fluid" src="img/clientes/ecovita.png" alt="">
                        </div>
                        <div>
                            <img class="img-fluid" src="img/clientes/rps.png" alt="">
                        </div>
                        <div>
                            <img class="img-fluid" src="img/clientes/kern.png" alt="">
                        </div>
                        <div>
                            <img class="img-fluid" src="img/clientes/htb.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php
    include "rodape.php";
?>
<?php
    include "cabecalho.php";
?>
    <section>
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="blog">
                    <h2 class="text-center"><b>Blog</b></h2>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="noticia">
                    <img src="img/noticias/noticia1.png" class="img-noticia" alt="">
                    <p class="noticia-head">Lorem ipsum dolor sit amet, consecte.</p>
                    <div class="text-right">
                        <a href="">Saber mais</a>
                    </div>
                </div>
                <div class="noticia">
                    <img src="img/noticias/noticia2.png" class="img-noticia" alt="">
                    <p class="noticia-head">Lorem ipsum dolor sit amet, consecte.</p>
                    <div class="text-right">
                        <a href="">Saber mais</a>
                    </div>
                </div>
                <div class="noticia">
                    <img src="img/noticias/noticia3.png" class="img-noticia" alt="">
                    <p class="noticia-head">Lorem ipsum dolor sit amet, consecte.</p>
                    <div class="text-right">
                        <a href="">Saber mais</a>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center mt-lg-5">
                <div class="noticia">
                    <img src="img/noticias/noticia1.png" class="img-noticia" alt="">
                    <p class="noticia-head">Lorem ipsum dolor sit amet, consecte.</p>
                    <div class="text-right">
                        <a href="">Saber mais</a>
                    </div>
                </div>
                <div class="noticia">
                    <img src="img/noticias/noticia2.png" class="img-noticia" alt="">
                    <p class="noticia-head">Lorem ipsum dolor sit amet, consecte.</p>
                    <div class="text-right">
                        <a href="">Saber mais</a>
                    </div>
                </div>
                <div class="noticia">
                    <img src="img/noticias/noticia3.png" class="img-noticia" alt="">
                    <p class="noticia-head">Lorem ipsum dolor sit amet, consecte.</p>
                    <div class="text-right">
                        <a href="">Saber mais</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="paginacao">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination justify-content-center">
                            <li class="page-item disabled">
                                <a class="page-link" href="#" tabindex="-1">Primeira</a>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item"><a class="page-link" href="#">4</a></li>
                            <li class="page-item active"><a class="page-link" href="#">5</a></li>
                            <li class="page-item"><a class="page-link" href="#">6</a></li>
                            <li class="page-item"><a class="page-link" href="#">7</a></li>
                            <li class="page-item"><a class="page-link" href="#">8</a></li>
                            <li class="page-item"><a class="page-link" href="#">9</a></li>
                            <li class="page-item">
                                <a class="page-link" href="#">Ultima</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </section>
<?php
    include "rodape.php";
?>
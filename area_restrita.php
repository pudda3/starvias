<?php
    include "cabecalho.php";
?>
    <section id="area">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="formulario">
                    <h3>Acompanhe sua obra de onde você estiver.</h3>
                    <div class="form-separador">
                         <img src="img/separador.png" class="img-fluid" alt="">
                    </div>
                    <form action="">
                        <div class="form-group">
                            <label class="ml-2" for="login">Login</label>
                            <input type="text" class="form-control campo-formulario" name="login" id="login" placeholder="Login">
                        </div>
                        <div class="form-group">
                            <label class="ml-2" for="senha">Senha</label>
                            <input type="text" class="form-control campo-formulario" name="senha" id="senha" placeholder="Senha">
                        </div>
                        <button class="botao-login">ENTRAR</button>
                        <button class="botao-cadastro mt-3">CADASTRAR</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
<?php
    include "rodape.php";
?>
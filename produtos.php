<?php
    include "cabecalho.php";
?>
    <section id="banner-produtos">
        <div class="container-fluid">
            <div class="row justify-items-center">
                <div class="banner-texto texto-direita">
                    <div><b>A linha de produtos Stavias é completa.</b></div>
                </div>
                <div class="banner-texto texto-esquerda">
                    <div>Tudo o que você precisa para sua obra.</div>
                </div>
            </div>
        </div>
    </section>
    <section class="my-4">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="round-icons flex-column flex-lg-row">
                    <div class="round-content">
                        <div class="redondo">
                            <img src="img/planta.png" alt="">
                        </div>
                        <p><b>Usina a gás,</b> equipada para asfalto borracha.</p>
                    </div>
                    <div class="round-content">
                        <div class="redondo">
                            <img src="img/estoque.png" alt="">
                        </div>
                        <p>Matéria-prima <b>com estocagem coberta.</b></p>
                    </div>
                    <div class="round-content">
                        <div class="redondo">
                            <img src="img/qualidade.png" alt="">
                        </div>
                        <p>Rigoroso <b>controle de qualidade.</b></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="section-break">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="imgs-break d-xl-flex justify-content-xl-around align-items-xl-center">
                    <div class="carousel-cell"><img src="img/imagem1.png" alt=""></div>
                    <div class="carousel-cell"><img src="img/imagem2.png" alt=""></div>
                    <div class="carousel-cell"><img src="img/imagem3.png" alt=""></div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="lista-produtos">
                    <ul>
                        <li>Areia grossa, média e fina</li>
                        <li>Usinados não betuminosos (usina de solo)</li>
                        <li class="enfase"><b>Pedra britada</b></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section id="produtos">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="produtos">
                    <div class="produto-foto">
                        <img src="img/produtos/caneta.png" alt="">
                        <b>&nbsp;</b>
                    </div>
                    <div class="produto-foto">
                        <img src="img/produtos/po-de-bitra.png" alt="">
                        <b>Pó de Brita</b>
                    </div>
                    <div class="produto-foto">
                        <img src="img/produtos/pedrisco.png" alt="">
                        <b>Pedrisco</b>
                    </div>
                    <div class="produto-foto">
                        <img src="img/produtos/brita-meia.png" alt="">
                        <b>Brita 1/2</b>
                    </div>
                    <div class="produto-foto">
                        <img src="img/produtos/brita1.png" alt="">
                        <b>Brita 1</b>
                    </div>
                    <div class="produto-foto">
                        <img src="img/produtos/brita2.png" alt="">
                        <b>Brita 2</b>
                    </div>
                    <div class="produto-foto d-none d-xl-flex">
                        <img src="img/produtos/caneta.png" alt="">
                        <b>&nbsp;</b>
                    </div>
                    <div class="produto-foto">
                        <img src="img/produtos/brita3.png" alt="">
                        <b>Brita 3</b>
                    </div>
                    <div class="produto-foto">
                        <img src="img/produtos/brita4.png" alt="">
                        <b>Brita 4</b>
                    </div>
                    <div class="produto-foto">
                        <img src="img/produtos/rachao.png" alt="">
                        <b>Rachão (ou pedra de mão)</b>
                    </div>
                    <div class="produto-foto">
                        <img src="img/produtos/brita1.png" alt="">
                        <b>Bica Corrida e Graduada</b>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php
    include "rodape.php";
?>
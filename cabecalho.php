<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Stavias</title>

    <!-- FAVICON METADATA -->
        
        <link rel="apple-touch-icon" sizes="180x180" href="img/favicons/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="img/favicons/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="192x192" href="img/favicons/android-chrome-192x192.png">
        <link rel="icon" type="image/png" sizes="16x16" href="img/favicons/favicon-16x16.png">
        <link rel="manifest" href="img/favicons/site.webmanifest">
        <link rel="mask-icon" href="img/favicons/safari-pinned-tab.svg" color="#004841">
        <link rel="shortcut icon" href="img/favicons/favicon.ico">
        <meta name="apple-mobile-web-app-title" content="Stavias">
        <meta name="application-name" content="Stavias">
        <meta name="msapplication-TileColor" content="#004841">
        <meta name="msapplication-config" content="img/favicons/browserconfig.xml">
        <meta name="theme-color" content="#004841">

    <!-- END FAVICON METADATA -->

    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/fontawesome.min.css">
    <link rel="stylesheet" href="assets/css/flickity.min.css">
    <link rel="stylesheet" href="node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.css"/>
</head>
<body>
    <div class="container-fluid cabecalho">
        <div class="row justify-content-center">
            <div class="d-flex justify-content-end align-items-center topo-contato">
                <div class="pr-3">
                    <a href="tel:+19935222830"><img src="img/phone.png" alt=""></a>
                    <span class="d-none d-md-inline-block">19 3522.2830</span>
                </div>
                <div>
                    <a href="mailto:stavias@stavias.com.br"><img src="img/envelope.png" alt=""></a>
                    <span class="d-none d-md-inline-block">stavias@stavias.com.br</span>
                </div>
            </div>
        </div>
    </div>
    <section class="topo">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <nav class="navbar navbar-expand-lg navbar-dark">
                    <a class="navbar-brand" href="#"><img src="img/logo.png"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <div class="separador"></div>
                            <li class="nav-item active">
                                <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
                            </li>
                            <div class="separador"></div>
                            <li class="nav-item">
                                <a class="nav-link" href="empresa.php">A Stavias</a>
                            </li>
                            <div class="separador"></div>
                            <li class="nav-item">
                                <a class="nav-link" href="servicos.php">Serviços</a>
                            </li>
                            <div class="separador"></div>
                            <li class="nav-item">
                                <a class="nav-link" href="produtos.php">Produtos</a>
                            </li>
                            <div class="separador"></div>
                            <li class="nav-item">
                                <a class="nav-link" href="responsabilidade.php">Responsabilidade Ambiental</a>
                            </li>
                            <div class="separador"></div>
                            <li class="nav-item">
                                <a class="nav-link" href="clientes.php">Nossos Clientes</a>
                            </li>
                            <div class="separador"></div>
                            <li class="nav-item">
                                <a class="nav-link" href="blog.php">Blog</a>
                            </li>
                            <div class="separador"></div>
                            <li class="nav-item">
                                <a class="nav-link" href="contato.php">Contato</a>
                            </li>
                            <div class="separador"></div>
                            <li class="nav-item">
                                <a class="nav-link" href="area_restrita.php" role="button"><span>Área Restrita</span></a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </section>
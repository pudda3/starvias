var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
const webpack = require('webpack-stream');

gulp.task('sass', function () {
    return gulp.src('./sass/**/*.sass')
    .pipe(sass({ outputStyle: 'compressed' }).on('error', sass.logError))
    .pipe(gulp.dest('./assets/css'));
});

gulp.task('sass:watch', function () {
    gulp.watch('./sass/**/*.sass', ['sass']);
});

gulp.task('webpack', function() {
    return gulp.src('./assets/js/*.js')
    .pipe(webpack( require('./webpack.config.js') ))
    .pipe(gulp.dest('dist/'));
});

gulp.task('webpack:watch', function () {
    gulp.watch('./assets/js/*.js', ['webpack']);
});

gulp.task('default', ['sass', 'sass:watch', 'webpack', 'webpack:watch']);
<footer>
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="rodape d-flex flex-column flex-lg-row justify-content-between align-items-start align-items-lg-center">
                    <div>
                        <img src="img/logo-footer.png" alt="">
                        <p class="pt-3 mb-0">&copy; Stavias - 2018</p>
                        <p>Todos os direitos reservados</p>
                        <i class="fab fa-facebook-square"></i>
                        <i class="fab fa-linkedin"></i>
                    </div>
                    <div>
                        <p class="texto-amarelo tel"><a href="tel:+1935222830">(19) 3522.2830<a></p>
                        <p class="pt-3 mb-0"><a href="mailto:stavias@stavias.com.br">stavias@stavias.com.br</a></p>
                        <p><a href="mailto:compras@stavias.com.br">compras@stavias.com.br</a></p>
                    </div>
                    <div>
                        <p class="pt-3 mb-0">Rodovia Fausto Santomauro</p>
                        <p class="mb-0">SP-127, Km 7 - Distrito da Assistência</p>
                        <p class="mb-0">Rio Claro-SP - Caixa Postal 283</p>
                        <p>CEP 13500 – 970</p>
                    </div>
                    <div>
                        <p class="mb-3"><strong>Newsletter</strong></p>
                        <p class="mb-0">Quer receber nossas novidades e notícias? </p>
                        <p>Inscreva-se</p>
                        <form action="">
                            <div class="form-group">
                                <input type="text" class="form-control mb-2" placeholder="Nome">
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control" placeholder="Seu melhor E-mail" aria-label="Recipient's username" aria-describedby="button-addon2">
                                    <div class="input-group-append">
                                        <button class="btn btn-outline-secondary btn-news" type="button" id="button-addon2"><i class="fab fa-telegram-plane"></i></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="diclaimer">
            <div class="text-right">
                <p>Site desenvolvido por: Álamo Comunicação</p>
            </div>
        </div>
        
    </footer>
    <!-- <script src="assets/js/jquery-3.3.1.min.js"></script> 
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="assets/js/fontawesome.min.js"></script>
    <script src="node_modules/flickity/dist/flickity.pkgd.min.js"></script>-->
    <script src="dist/bundle.js"></script>
</body>

</html>
window.$ = window.jQuery = require('jquery');
require('bootstrap');
var jQueryBridget = require('jquery-bridget');
var Flickity = require('flickity');
require('../../assets/js/fontawesome.min.js');
require('../../assets/js/numscroller.js');
require('@fancyapps/fancybox')

Flickity.setJQuery( $ );
jQueryBridget( 'flickity', Flickity, $ );

$('.main-carousel').flickity({
    prevNextButtons: false,
    wrapAround: true,
    draggable: false,
    autoPlay: 5000,
    adaptiveHeight: true
});

$('.servico').hover(function (){
    $('#'+this.id+' img').attr('src', url+'/assets/img/'+$('#'+this.id+' img').attr('id')+'-black.png');

    $('#'+this.id+' .bg-yellow').css({
        transition: 'all 0.5s',
        height: '100%'
    });

    $('#'+this.id+' .content a').css({
        color: '#fff'
    });

    $('#'+this.id+' .content').css({
        transition: 'all 0.5s',
        'background-color': 'transparent'
    });

    $('#'+this.id+' .content span').css({
        'color': '#000',
        'font-weight': '800'
    });
},function (){
    $('#'+this.id+' img').attr('src', url+'/assets/img/'+$('#'+this.id+' img').attr('id')+'-yellow.png');

    $('#'+this.id+' .bg-yellow').css({
        transition: 'all 1s',
        height: '0px'
    });
    $('#'+this.id+' .content a').css({
        color: '#ffdd00'
    });
    $('#'+this.id+' .content').css({
        transition: 'all 1s',
        'background-color': 'rgba(65,65,65,0.7)'
    });
    $('#'+this.id+' .content span').css({
        'color': '#fff',
        'font-weight': '500'
    });
});

$('.content a').on('click', function () {
    var id = $(this).closest('.celula').attr('id');
    $('#img-historia').animate({opacity : 0}, 250, function() {
        $('#img-historia').attr('src', url+'/assets/img/historia/historia-'+id+'.png');
        $('#img-historia').animate({opacity : 1}, 350);
    });
    
    $('.celula').removeClass('selected');
    $('.celula#'+id).toggleClass('selected');
});

var w = $(window).width();

if (w < 1199) {
    $(".imgs-break").flickity({
        wrapAround: true,
        draggable: true,
        // autoPlay: 5000,
        adaptiveHeight: true
    });
}

$('.carrossel').flickity({
    groupCells: true
});
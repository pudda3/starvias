<?php
    include "cabecalho.php";
?>
    <section id="servicos-banner">
    </section>
    <section id="servicos-icones">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="servicos-grid">
                    <div class="servico">
                        <div class="icone-serv">
                            <img src="img/servicos/terraplenagem.png" alt="">
                        </div>
                        <div class="">
                            <p>
                                Terraplenagem<br>
                                Pavimentação<br>
                                Guias e sarjetas<br>
                            </p>
                        </div>
                    </div>
                    <div class="servico">
                        <div class="icone-serv">
                            <img src="img/servicos/encanamento.png" alt="">
                        </div>
                        <div class="">
                            <p>
                                Rede de drenagem<br>
                                Rede de esgoto<br>
                                Rede de água potável<br>
                            </p>
                        </div>
                    </div>
                    <div class="servico">
                        <div class="icone-serv">
                            <img src="img/servicos/recuperacao.png" alt="">
                        </div>
                        <div class="">
                            <p>
                                Recuperação de áreas degradadas
                            </p>
                        </div>
                    </div>
                    <div class="servico">
                        <div class="icone-serv">
                            <img src="img/servicos/canalizacao.png" alt="">
                        </div>
                        <div class="">
                            <p>
                                Canalização de cursos d’água
                                Desassoreamento de cursos d’água
                            </p>
                        </div>
                    </div>
                    <div class="servico">
                        <div class="icone-serv">
                            <img src="img/servicos/muro.png" alt="">
                        </div>
                        <div class="">
                            <p>
                                Obras de arte<br>
                                Muro de contenção<br>
                            </p>
                        </div>
                    </div>
                    <div class="servico">
                        <div class="icone-serv">
                            <img src="img/servicos/demolicao.png" alt="">
                        </div>
                        <div class="">
                            <p>
                                Demolição mecanizada<br>
                                Desmonte de rocha<br>
                                Sondagem geológica<br>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="detalhes-servicos">
        <div class="container-fluid py-5 bg-cinza-fraco">
            <div class="row justify-content-center ">
                <div class="detalhe">
                    <div class="detalhe-texto">
                        <h2 class="texto-verde">Terraplenagem</h2>
                        <p>Com rigoroso controle tecnológico e equipe altamente capacitada para oferecer soluções construtivas dentro das melhores práticas, a Stavias realiza cuidadosamente todas as etapas da Terraplenagem, da limpeza e escavação do terreno, com carga e transporte de materiais, à adequação da superfície ao projeto dimensionado, sejam pátios industriais, arruamentos, barragens, rodovias ou outros.</p>
                    </div>
                    <div class="detalhe-imagem d-none d-lg-block">
                        <a data-fancybox="gallery" href="img/servicos/servico1-big.png"><img class="img-fluid" src="img/servicos/servico1.png"></a>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="servico-galeria">
                    <div class="carrossel">
                        <div class="carousel-cell col-10 col-md-6 col-lg-3">
                            <a data-fancybox="gallery" href="img/servicos/servico2-big.png"><img class="img-fluid" src="img/servicos/servico2.png"></a>
                        </div>
                        <div class="carousel-cell col-10 col-md-6 col-lg-3">
                            <a data-fancybox="gallery" href="img/servicos/servico3-big.png"><img class="img-fluid" src="img/servicos/servico3.png"></a>
                        </div>
                        <div class="carousel-cell col-10 col-md-6 col-lg-3">
                            <a data-fancybox="gallery" href="img/servicos/servico4-big.png"><img class="img-fluid" src="img/servicos/servico4.png"></a>
                        </div>
                        <div class="carousel-cell col-10 col-md-6 col-lg-3">
                            <a data-fancybox="gallery" href="img/servicos/servico5-big.png"><img class="img-fluid" src="img/servicos/servico5.png"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid py-5">
            <div class="row justify-content-center ">
                <div class="detalhe">
                    <div class="detalhe-texto">
                        <h2 class="texto-verde">Pavimentação </h2>
                        <p>Atendendo às mais exigentes demandas de cada tipo de projeto, a Stavias conta com uma equipe técnica própria e altamente treinada, além de produção de matérias e equipamentos próprios para projetos de pavimentação, realizando execução de subleito, base, imprimações, impermeabilizantes e ligante, além da camada de revestimento.</p>
                    </div>
                    <div class="detalhe-imagem d-none d-lg-block">
                        <a data-fancybox="gallery" href="img/servicos/servico1-big.png"><img class="img-fluid" src="img/servicos/servico1.png"></a>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="servico-galeria">
                    <div class="carrossel">
                        <div class="carousel-cell col-10 col-md-6 col-lg-3">
                            <a data-fancybox="gallery" href="img/servicos/servico2-big.png"><img class="img-fluid" src="img/servicos/servico2.png"></a>
                        </div>
                        <div class="carousel-cell col-10 col-md-6 col-lg-3">
                            <a data-fancybox="gallery" href="img/servicos/servico3-big.png"><img class="img-fluid" src="img/servicos/servico3.png"></a>
                        </div>
                        <div class="carousel-cell col-10 col-md-6 col-lg-3">
                            <a data-fancybox="gallery" href="img/servicos/servico4-big.png"><img class="img-fluid" src="img/servicos/servico4.png"></a>
                        </div>
                        <div class="carousel-cell col-10 col-md-6 col-lg-3">
                            <a data-fancybox="gallery" href="img/servicos/servico5-big.png"><img class="img-fluid" src="img/servicos/servico5.png"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid py-5 bg-cinza-fraco">
            <div class="row justify-content-center">
                <div class="detalhe">
                    <div class="detalhe-texto">
                        <h2 class="texto-verde">Redes de Drenagem, de Esgoto e de Água Potável</h2>
                        <p>A vantagem de contar com equipamentos próprios e produção própria de matérias, garante a resistência e a qualidade de acabamento dos mais diversos tipos de redes executados pela Stavias, que preza pelo respeito ao cronograma do cliente. Com soluções versáteis e econômicas, a Stavias ainda realiza reaterro controlado com auxílio de laboratório e teste de estanqueidade.</p>
                    </div>
                    <div class="detalhe-imagem d-none d-lg-block">
                        <a data-fancybox="gallery" href="img/servicos/servico1-big.png"><img class="img-fluid" src="img/servicos/servico1.png"></a>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="servico-galeria">
                    <div class="carrossel">
                        <div class="carousel-cell col-10 col-md-6 col-lg-3">
                            <a data-fancybox="gallery" href="img/servicos/servico2-big.png"><img class="img-fluid" src="img/servicos/servico2.png"></a>
                        </div>
                        <div class="carousel-cell col-10 col-md-6 col-lg-3">
                            <a data-fancybox="gallery" href="img/servicos/servico3-big.png"><img class="img-fluid" src="img/servicos/servico3.png"></a>
                        </div>
                        <div class="carousel-cell col-10 col-md-6 col-lg-3">
                            <a data-fancybox="gallery" href="img/servicos/servico4-big.png"><img class="img-fluid" src="img/servicos/servico4.png"></a>
                        </div>
                        <div class="carousel-cell col-10 col-md-6 col-lg-3">
                            <a data-fancybox="gallery" href="img/servicos/servico5-big.png"><img class="img-fluid" src="img/servicos/servico5.png"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="bg-amarelo ">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="servico-contato">
                    <a href="contato.php">ENTRE EM CONTATO</a>
                </div>
            </div>
        </div>
    </section>
    <section id="alugel">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="alugel-info">
                    <h2 class="texto-verde">Locação de Máquinas</h2>
                    <p>A facilidade e a segurança de contar com os equipamentos mais atualizados do mercado, pelo tempo que você precisa, para a obra que você está executando.</p>
                </div>
            </div>
            <div class="row justify-content-center py-3">
                <div class="alugel-grid">
                    <div class="alugel-filtros">
                        <h5 class="texto-verde">Filtrar resultados</h5>
                            <ul>
                                <li>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="carregar">
                                        <label class="form-check-label" for="carregar">
                                            Carregar
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="compactar">
                                        <label class="form-check-label" for="compactar">
                                            Compactar
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="comprimir">
                                        <label class="form-check-label" for="comprimir">
                                            Comprimir
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="demolir">
                                        <label class="form-check-label" for="demolir">
                                            Demolir
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="escavar">
                                        <label class="form-check-label" for="escavar">
                                            Escavar
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="nivelar">
                                        <label class="form-check-label" for="nivelar">
                                            Nivelar
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="pavimentar">
                                        <label class="form-check-label" for="pavimentar">
                                            Pavimentar
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="transportar">
                                        <label class="form-check-label" for="transportar">
                                            Transportar
                                        </label>
                                    </div>
                                </li>
                            </ul>
                    </div>
                    <div class="alugel-item item-1">
                        <div>
                            <div class="bg-gray">
                                <h6 class="texto-verde">Mini Rolo Compactador</h6>
                            </div>
                            <div class="item-detalhes">
                                <div class="item-imagem">
                                    <img src="img/maquinas/rolo-compactador.png" class="img-fluid-reverse" alt="">
                                </div>
                                <div>
                                    <hr>
                                    <p>Modelo: <b>CC 900G</b></p>
                                    <p>Fabricante: <b>DYNAPAC</b></p>
                                    <hr>
                                    <p>Potência bruta: 18 cv</p>
                                    <p>Largura do rolo: 889 mm</p>
                                    <p>Peso de operação: 1.335 kg</p>
                                </div>
                            </div>
                        </div>
                        <div class="item-botao bg-amarelo text-center p-3 texto-verde">
                            <a href="">Alugar essa máquina</a>
                        </div>
                    </div>
                    <div class="alugel-item item-2">
                        <div>
                            <div class="bg-gray">
                                <h6 class="texto-verde">Comboio Aspargidor de Asfalto</h6>
                            </div>
                            <div class="item-detalhes">
                                <div class="item-imagem">
                                    <img src="img/maquinas/aspargidor-asfalto.png" class="img-fluid-reverse" alt="">
                                </div>
                                <div>
                                    <hr>
                                    <p>Modelo: <b>CARGO 2423</b></p>
                                    <p>Fabricante: <b>JCAPACLE (FORD)</b></p>
                                    <hr>
                                    <p>Capacidade do tanque: 12.000 L</p>
                                    <p>Tração: 6 x 4</p>
                                </div>
                            </div>
                        </div>
                        <div class="item-botao bg-amarelo text-center p-3 texto-verde">
                            <a href="">Alugar essa máquina</a>
                        </div>
                    </div>
                    <div class="alugel-item item-3">
                        <div>
                            <div class="bg-gray">
                                <h6 class="texto-verde">Rolo Compactador Tandem</h6>
                            </div>
                            <div class="item-detalhes">
                                <div class="item-imagem">
                                    <img src="img/maquinas/compactador-tandem.png" class="img-fluid-reverse" alt="">
                                </div>
                                <div>
                                    <hr>
                                    <p>Modelo: <b>CC 424HF</b></p>
                                    <p>Fabricante: <b>DYNAPAC</b></p>
                                    <hr>
                                    <p>Potência Bruta: 110 cv</p>
                                    <p>Peso de Operação: 10.000 kg</p>
                                    <p>Largura do Rolo: 1.700 mm</p>
                                </div>
                            </div>
                        </div>
                        <div class="item-botao bg-amarelo text-center p-3 texto-verde">
                            <a href="">Alugar essa máquina</a>
                        </div>
                    </div>
                    <div class="alugel-item item-4">
                        <div>
                            <div class="bg-gray">
                                <h6 class="texto-verde">Caminhão Munck</h6>
                            </div>
                            <div class="item-detalhes">
                                <div class="item-imagem">
                                    <img src="img/maquinas/munck.png" class="img-fluid-reverse" alt="">
                                </div>
                                <div>
                                    <hr>
                                    <p>Modelo: <b>PHD 40007 CARGO 2423</b></p>
                                    <p>Fabricante: <b>PHD (FORD)</b></p>
                                    <hr>
                                    <p>Capacidade máxima: 40.000 kgf/m</p>
                                    <p>Quantidade de lanças: 04 lanças</p>
                                </div>
                            </div>
                        </div>
                        <div class="item-botao bg-amarelo text-center p-3 texto-verde">
                            <a href="">Alugar essa máquina</a>
                        </div>
                    </div>
                    <div class="alugel-item item-5">
                        <div>
                            <div class="bg-gray">
                                <h6 class="texto-verde">Prancha Carrega-Tudo</h6>
                            </div>
                            <div class="item-detalhes">
                                <div class="item-imagem">
                                    <img src="img/maquinas/carrega-tudo.png" class="img-fluid-reverse" alt="">
                                </div>
                                <div>
                                    <hr>
                                    <p>Modelo: <b>REBAIXADO</b></p>
                                    <p>Fabricante: <b>Truck São João</b></p>
                                    <hr>
                                    <p>Eixos: 03</p>
                                    <p>Carga útil: 30.000 kg</p>
                                    <p>Dimensões: 3,0x19,4 m</p>
                                </div>
                            </div>
                        </div>
                        <div class="item-botao bg-amarelo text-center p-3 texto-verde">
                            <a href="">Alugar essa máquina</a>
                        </div>
                    </div>
                    <div class="alugel-item item-6">
                        <div>
                            <div class="bg-gray">
                                <h6 class="texto-verde">Caminhões</h6>
                            </div>
                            <div class="item-detalhes">
                                <div class="item-imagem">
                                    <img src="img/maquinas/caminhao.png" class="img-fluid-reverse" alt="">
                                </div>
                                <div>
                                    <hr>
                                    <p>Modelo: <b>VW 31320</b></p>
                                    <p>Fabricante: <b>Volkswagen</b></p>
                                    <hr>
                                    <p>Tração: 6 x 4</p>
                                    <p>Capacidade cúbica de transporte: 12 à 14 m³</p>
                                </div>
                            </div>
                        </div>
                        <div class="item-botao bg-amarelo text-center p-3 texto-verde">
                            <a href="">Alugar essa máquina</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php
    include "rodape.php";
?>
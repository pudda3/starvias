<?php
    include "cabecalho.php";
?>
    <section class="bg-amarelo" id="banner-empresa">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="banner-empresa">
                    <div class="texto-esquerda">
                        <p><b class="texto-verde">Inovação.</b> Uma palavra que resume bem o que a Stavias busca todo dia.<b> Por isso, é referência</b> no setor de terraplenagem e pavimentação. </p>
                    </div>
                    <div class="texto-direita">
                        <p>Como uma empresa familiar, a Stavias se fundamenta na tradição e profissionalismo, mas também se mantém atenta à evolução de novos equipamentos e metodologias construtivas, com foco no aumento da competitividade e eficiência em obras de terraplenagem, pavimentação e infraestrutura em geral. Desse esforço contínuo, resultou a capacidade de trabalhar em todas as etapas deste tipo de obras, seja no projeto, planejamento, execução, fornecimento de matérias-primas, gestão de custos e pessoas com ênfase no atendimento dos requisitos regulamentares da construção civil, técnicos ou legais.</p>
                        <p>Para sustentar este nível de especialização, a Stavias busca sempre manter e incrementar alguns diferenciais, como a localização estratégica, a logística própria e a produção de pedra e de concreto asfáltico também própria, o que garante autonomia, flexibilidade e total controle sobre a qualidade dos materiais que utiliza.</p>
                        <p>Além da eficiência, uma gestão cada vez mais transparente de contratos é prioridade, para disponibilizar ao cliente ferramentas digitais para que ele mesmo possa comodamente acompanhar o andamento de cada etapa do seu projeto. Afinal, negócios em construção exigem confiabilidade e solidez. </p>
                        <p><strong class="texto-verde">A Stavias caminha no chão firme que ela mesma constrói, dia após dia.</strong></p>
                        <a href="contato.php" class="btn-banner">ENTRE EM CONTATO</a>
                    </div>
                </div>
            </div>
        </div>
        <img class="img-banner" src="img/banner-caminhao.png" alt="">
    </section>
    <section id="dados-empresa">
        <div class="container-fluid">
            <div class="row justify-content-center align-items-center">
                <div class="numeros">
                    <div>
                        <b class="texto-verde">Números</b>
                    </div>
                    <div class="d-flex flex-column flex-lg-row justify-content-around align-items-center align-items-lg-start">
                        <div class="num-cel">
                            <span class='numscroller' data-min='550' data-max='608' data-delay='5' data-increment='5'>1000</span>
                            <span class='legenda'>Obras executadas</span>
                        </div>
                        <div class="num-cel">
                            <span class='numscroller' data-min='5157942' data-max='5158000' data-delay='5' data-increment='5'>5.158.000</span>
                            <span class='legenda'>m&sup2; pavimentados</span>
                        </div>
                        <div class="num-cel">
                            <span class='numscroller' data-min='3231942' data-max='3232000' data-delay='5' data-increment='5'>3.232.000</span>
                            <span class='legenda'>m&sup3; de terra movimentada</span>
                        </div>
                        <div class="num-cel">
                            <span class='numscroller' data-min='2234942' data-max='2235000' data-delay='5' data-increment='5'>2.235.000</span>
                            <span class='legenda'>m de tubulação de água, <br>esgoto e drenagem. <br>(área administrável)</span>
                        </div>                            
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="historia-empresa">
        <div class="container-fluid">
            <div class="row justify-content-center align-items-center flex-column">
                <div class="col-10 col-xl-8 p-0 py-5">
                    <b class="texto-verde">Historia</b>
                </div>
                <div class="historia d-flex justify-content-between">
                    <div class="timeline">
                        <div class="celula left selected" id="2014">
                            <div class="content">
                                <a>2014</a>
                            </div>
                        </div>
                        <div class="celula left" id="2013">
                            <div class="content">
                                <a>2013</a>
                            </div>
                        </div>
                        <div class="celula left" id="2012">
                            <div class="content">
                                <a>2012</a>
                            </div>
                        </div>
                        <div class="celula left" id="2011">
                            <div class="content">
                                <a>2011</a>
                            </div>
                        </div>
                        <div class="celula left" id="2010">
                            <div class="content">
                                <a>2010</a>
                            </div>
                        </div>
                        <div class="celula left" id="2009">
                            <div class="content">
                                <a>2009</a>
                            </div>
                        </div>
                        <div class="celula left" id="2008">
                            <div class="content">
                                <a>2008</a>
                            </div>
                        </div>
                        <div class="celula left" id="2006">
                            <div class="content">
                                <a>2006</a>
                            </div>
                        </div>
                        <div class="celula left" id="2000">
                            <div class="content">
                                <a>2000</a>
                            </div>
                        </div>
                        <div class="celula left" id="1999">
                            <div class="content">
                                <a>1999</a>
                            </div>
                        </div>
                        <div class="celula left" id="1995">
                            <div class="content">
                                <a>1995</a>
                            </div>
                        </div>
                        <div class="celula left" id="1994">
                            <div class="content">
                                <a>1994</a>
                            </div>
                        </div>
                        <div class="celula left" id="1993">
                            <div class="content">
                                <a>1993</a>
                            </div>
                        </div>
                        <div class="celula left" id="1992">
                            <div class="content">
                                <a>1992</a>
                            </div>
                        </div>
                        <div class="celula left" id="1989">
                            <div class="content">
                                <a>1989</a>
                            </div>
                        </div>
                        <div class="celula left" id="1988">
                            <div class="content">
                                <a>1988</a>
                            </div>
                        </div>
                    </div>
                    <div class="historia-foto">
                        <img class='img-fluid' id="img-historia" src="img/historia/historia-2014.png">
                        <p class="mt-3">Início da operação de duas usinas de asfalto e concreto, com produtos e serviços de alta competitividade.</p>
                    </div>
                </div>
            </div>
            
        </div>
    </section>
<?php
    include "rodape.php";
?>
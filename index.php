<?php
    include "cabecalho.php";
?>
<section id="banner">
    <div class="main-carousel">
        <div class="carousel-cell">
            <div class="texto-banner">
                <h1 class="texto-amarelo">Soluções versáteis para a execução</h1>
                <h1 class="pb-3 pb-xl-5">de obras de infraestrutura</h1>
                <a href="contato.php" class="btn-banner text-uppercase">Entre em contato</a>
            </div>
            <img class="img-responsive" src="img/banner.png" alt="">
        </div>
        <div class="carousel-cell">
            <div class="texto-banner">
                <h1 class="texto-amarelo">Soluções versáteis para a execução</h1>
                <h1 class="pb-3 pb-xl-5">de obras de infraestrutura</h1>
                <a href="contato.php" class="btn-banner text-uppercase">Entre em contato</a>
            </div>
            <img class="img-responsive" src="img/banner.png" alt="">
        </div>
        <div class="carousel-cell">
            <div class="texto-banner">
                <h1 class="texto-amarelo">Soluções versáteis para a execução</h1>
                <h1 class="pb-3 pb-xl-5">de obras de infraestrutura</h1>
                <a href="contato.php" class="btn-banner text-uppercase">Entre em contato</a>
            </div>
            <img class="img-responsive" src="img/banner.png" alt="">
        </div>
    </div>
</section>
<section id="servicos">
    <div class="container-fluid">
        <div class="row justify-content-center align-items-strecht">
            <div class="titulo-servicos text-center">
                <h3>O que você procura?</h3>
            </div>
            <div class="servicos justify-content-center justify-content-lg-between" >
                <div class="servico" id="001">
                    <div class="bgzoom gestao"></div>
                    <div class="content">
                        <img class="icone-servico" id='gestao_obras' src="img/gestao_obras-yellow.png" alt="">
                        <span>Execução e gestão de obras de infraestrutura</span>
                        <a href="">veja mais</a>
                    </div>
                    <div class="bg-yellow"></div>
                </div>
                <div class="servico" id="002">
                    <div class="bgzoom pedra"></div>
                    <div class="content">
                        <img class="icone-servico" id="pedra-britada" src="img/pedra-britada-yellow.png" alt="">
                        <span>Pedra britada, areia, usinados</span>
                        <a href="">veja mais</a>
                    </div>
                    <div class="bg-yellow"></div>
                </div>
                <div class="servico" id="003">
                    <div class="bgzoom locacao"></div>
                    <div class="content">
                        <img class="icone-servico" id="locacao-maquinas" src="img/locacao-maquinas-yellow.png" alt="">
                        <span>Locação de máquinas</span>
                        <a href="">veja mais</a>
                    </div>
                    <div class="bg-yellow"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="lista-servicos">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="listas d-flex flex-column flex-lg-row justify-content-between align-items-stretch">
                    <ul>
                        <li><b class="texto-amarelo">Tradição de 50 anos</b> <span>no segmento</span></li>
                        <li><span>Do planejamento à execução de obras</span></li>
                        <li><span>Do planejamento à execução de obras</span></li>
                        <li><span>Controle e otimização dos recursos de projetos</span></li>
                        <li><span>Acompanhamento de prazos</span></li>
                        <li><span>Treinamento contínuo da equipe</span></li>
                        <li><span>Logística</span></li>
                    </ul>
                    <ul>
                        <li><span>Máquinas e equipamentos</span></li>
                        <li><span>Licenças exigidas (legislação)</span></li>
                        <li><span>Lavra e britagem</span></li>
                        <li><span>Localização privilegiada garante agilidade no fornecimento</span></li>
                        <li><span>Usina de massa asfáltica</span></li>
                        <li><span>Usina de concreto</span></li>
                        <li><span>Rigoroso controle de qualidade</span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
    include "rodape.php";
?>
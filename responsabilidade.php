<?php
    include "cabecalho.php";
?>
    <section id="resp-banner">
    </section>
    <section id="resp-conteudo">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="conteudo">
                    <div class="resp-icon">
                        <div class="redondo">
                            <img src="img/helmet.png" alt="">
                        </div>
                    </div>
                    <div class="resp-content">
                        <h2>Saúde no trabalho</h2>
                        <p>A Stavias despende especial atenção à saúde ocupacional. É uma das primeiras empresas a implantar, na região, a CIPAMIN (Comissão Interna de Prevenção a Acidentes na Mineração). Amparada por seus técnicos de segurança e empresas especializadas em Saúde e Engenharia do Trabalho, controla e monitora continuamente os seus riscos.</p>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center dark-bg">
                <div class="conteudo">
                    <div class="resp-icon">
                        <div class="redondo">
                            <img src="img/planta.png" alt="">
                        </div>
                    </div>
                    <div class="resp-content">
                        <h2>Recuperação de áreas degradadas</h2>
                        <p>Ainda na década de 1970, quando a questão ambiental era incipiente, o trabalho de recuperação da paisagem foi iniciado com um pequeno viveiro interno, para viabilizar o plantio de espécies arbóreas nativas na mata ciliar e barreira vegetal, inexistentes na época. No início da década de 1990, a Stavias já possuía o PRAD (Plano de Recuperação de Áreas Degradadas) aprovado.</p>
                        <p>Hoje, a mata ciliar excede a sua extensão regulamentar e a Reserva Legal aprovada fortalecem o ecossistema local, na bacia hidrográfica do Rio Corumbataí, melhorando as condições hidrogeoquímicas dos rios e formando um corredor para algumas espécies animais da região.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php
    include "rodape.php";
?>